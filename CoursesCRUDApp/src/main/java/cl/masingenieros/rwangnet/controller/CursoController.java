package cl.masingenieros.rwangnet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.masingenieros.rwangnet.model.Curso;
import cl.masingenieros.rwangnet.service.ICursoService;

/**
 * Controller que define la configuracion y endpoint de las operaciones CRUD
 * 
 * @author rwangnet
 *
 */
@RestController
@RequestMapping("/cursos")
public class CursoController {

	@Autowired
	private ICursoService service;

	@GetMapping
	public List<Curso> listar() {
		return service.listar();
	}

	@GetMapping(value = "/{id}")
	public Curso listar(@PathVariable("id") Integer id) {
		Curso c = service.listarPorId(id);
		return c != null ? c : new Curso();
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Curso registrar(@RequestBody Curso c) {
		return service.registrar(c);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Curso modificar(@RequestBody Curso c) {
		return service.modificar(c);
	}

	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Curso c = service.listarPorId(id);
		if (c != null) {
			Curso cur = new Curso();
			cur.setIdCurso(id);
			service.eliminar(cur);
			return 1;
		}
		return 0;
	}

}
