package cl.masingenieros.rwangnet.service;

import java.util.List;

import cl.masingenieros.rwangnet.model.Curso;

/**
 * 
 * @author rwangnet
 *
 */
public interface ICursoService {
	
	List<Curso> listar();
	Curso listarPorId(Integer id);
	Curso registrar(Curso c);
	Curso modificar(Curso c);
	void eliminar(Curso c);

}
