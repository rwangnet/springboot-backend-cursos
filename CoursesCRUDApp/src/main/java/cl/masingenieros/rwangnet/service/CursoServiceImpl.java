package cl.masingenieros.rwangnet.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.masingenieros.rwangnet.dao.ICursoDAO;
import cl.masingenieros.rwangnet.model.Curso;

/**
 * @author rwangnet
 */
@Service
public class CursoServiceImpl implements ICursoService {

	@Autowired
	private ICursoDAO dao;

	@Override
	public List<Curso> listar() {
		return dao.findAll();
	}

	@Override
	public Curso listarPorId(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Curso registrar(Curso c) {
		return dao.save(c);
	}

	@Override
	public Curso modificar(Curso c) {
		Curso curso = dao.findOne(c.getIdCurso());

		if (curso != null) {
			return dao.save(c);
		}

		return new Curso();
	}

	@Override
	public void eliminar(Curso c) {
		dao.delete(c);
	}

}
