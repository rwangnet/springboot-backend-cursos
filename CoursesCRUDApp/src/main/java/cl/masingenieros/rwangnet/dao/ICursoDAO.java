package cl.masingenieros.rwangnet.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.masingenieros.rwangnet.model.Curso;

/**
 * Interfaz que ofrece la posibilidad de que Spring (Data JPA) me cree la impl
 * on the fly, baby!
 * 
 * @author rwangnet
 *
 */
public interface ICursoDAO extends JpaRepository<Curso, Integer> {

}
